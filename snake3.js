window.onload = function()
{
	/* ----------------- HTML5 Canvas Setup* ----------------- */
	var snakeCanvas = document.createElement('canvas');
	var context = snakeCanvas.getContext('2d');

	var body = document.body;
    var html = document.documentElement;
    //raw document height and width
    var height = Math.max( body.scrollHeight, body.offsetHeight, 
                       html.clientHeight, html.scrollHeight, html.offsetHeight );
    var width = Math.max( body.scrollWidth, body.offsetWidth, 
	                   html.clientWidth, html.scrollWidth, html.offsetWidth );

    //doc height set to fit grid
    var finHeight = parseInt(Math.round(height/20) * 20 - 40);
    var finWidth = parseInt(Math.round(width/20) * 20 - 60);

	snakeCanvas.width = finWidth + 14;
	snakeCanvas.height = finHeight + 34;

	//appends canvas to page body tag
	var body = document.getElementsByTagName('body')[0];
	body.appendChild(snakeCanvas);

	function drawArena()
	{
		//draw border
		context.lineWidth = 2;
		context.strokeStyle = 'black';
		context.strokeRect(2, 20, snakeCanvas.width - 14, snakeCanvas.height - 34);
		//draw score and level 
		//context.fillStyle('black');
		//context.fillText('Score: ' + score + '    Level: ' + level, 2, 12);
	}

	/* ----------------- Map Logic  ----------------- */
	var map;
	function createMapGrid()
	{
		//construct 2D array for map
		map = new Array(parseInt(finWidth/10));
		for(var i = 0; i < map.length; i++)
		{
			map[i] = new Array(parseInt(finHeight/10));
		}
		console.log("window: " + finWidth/10 + ", " + finHeight/10);
	}

	function renderMap()
	{
		for(var x = 0; x < map.length; x++)
		{
			for(var y = 0; y < map[0].length; y++)
			{
				if(map[x][y] === 2)
				{
					for(var i = 0; i < snakeCollection.length; i++)
					{
						var snake = snakeCollection[i];
						for(var j = 0; j < snake.length; j++)
						{
							if(x === snake[j].x && y === snake[j].y)
							{
							context.fillStyle = snake[0].c;
							context.fillRect(x*10, y*10 + 20, 10, 10);
							//console.log(snake[0].c);
							}
						}
					}
				}
				if(map[x][y] === 1)
				{
					context.fillStyle = 'orange';
					context.fillRect(x*10, y*10 + 20, 10, 10);
				}
			}
		}
	}

	/* ----------------- Snake Logic ----------------- */
	var snakeCollection = new Array();
	function createSnake(ID, color)
	{
		var snakeCreated = new Array(5);

		var rX = Math.round(Math.random() * 19);
		var rY = Math.round(Math.random() * 19);

		//prevents being placed oob
		while(rX - snakeCreated.length < 0)
		{
			//console.log("stuff");
			rX = Math.round(Math.random() * 19);
		}
		//console.log(ID);
		//puts snake on map
		for(var i = 0; i < snakeCreated.length; i++)
		{
			snakeCreated[i] = 
			{
				x: rX - i,
				y: rY,
				d: 0,
				id: ID,
				c: color
			};

			map[rX - i][rY] = 2;
		}

		snakeCollection.push(snakeCreated);
		return snakeCreated;
	}

	function detectCollide()
	{
		for(var i = 0; i < snakeCollection.length; i++)
		{
			var snake = snakeCollection[i];
			if(snake[0].x < 0 || snake[0].x > (finWidth/10 - 1) ||
				snake[0].y < 0 || snake[0].y > (finHeight/10) - 1)
			{
				//console.log("bound!");

				for(var j = 0; j < snake.length; j++)
				{
					if(snake[j].x >= 0 && snake[j].y >= 0 &&
					snake[j].x < map.length && snake[j].y < map[j].length)
					{
						map[snake[j].x][snake[j].y] = null;
					}
				}
				snakeCollection.splice(i,1);
			}
		}
	}

	function detectSnake(i)
	{
		var snake = snakeCollection[i];

		if(snake[0].x >= 0 && snake[0].y >= 0 &&
			snake[0].x < map.length && snake[0].y < map[0].length)
		{
			if(map[snake[0].x][snake[0].y] === 2)
			{
				//console.log("collide");
				//console.log(i);
				
				for(var j = 0; j < snake.length; j++)
				{
					if(snake[j].x >= 0 && snake[j].y >= 0 &&
					snake[j].x < map.length && snake[j].y < map[j].length)
					{
						map[snake[j].x][snake[j].y] = null;
					}
				}
				
				//console.log(snakeCollection.length);
				snakeCollection.splice(i,1);
				//console.log(snakeCollection.length);
			}
		}
	}

	function detectFood()
	{
		for(var i = 0; i < snakeCollection.length; i++)
		{
			var snake = snakeCollection[i];
			//console.log(snake[0].x);

			if(snake[0].x >= 0 && snake[0].y >= 0 &&
				snake[0].x < map.length && snake[0].y < map[0].length)
			{
				//console.log("within");
				if(map[snake[0].x][snake[0].y] === 1)
				{
					//console.log("eat");
					createFood();
					snake.push({
						x: snake[snake.length - 1].x, 
						y: snake[snake.length - 1].y
					});
					console.log(snake.length);
					map[snake[snake.length - 1].x][snake[snake.length -1].y] = 2;
				}
			}
		}
	}


	/* ----------------- Food Logic ----------------- */
	function createFood()
	{
		var rX = Math.round(Math.random() * (map.length - 1))
		var rY = Math.round(Math.random() * (map[0].length - 1))

		while(map[rX][rY] === 2)
		{
			rX = Math.round(Math.random() * (map.length - 1))
			rY = Math.round(Math.random() * (map[0].length - 1))
		}

		map[rX][rY] = 1;
	}


	/* ----------------- Game Logic ----------------- */
	var score = 0;
	var level = 0;
	//need to make this an instance variable
	//var direction = 0;
	var slp = 125;

	createMapGrid();

	createFood();

	//createSnake();
	//createSnake();
	runGame();

	function runGame()
	{
		context.clearRect(0,0, snakeCanvas.width, snakeCanvas.height);

		drawArena();

		detectCollide();
		movePieces();
		detectCollide();
		
		renderMap();
		//renderMap();

		setTimeout(runGame, slp);
	}

	function movePieces()
	{
		for(var i = 0; i < snakeCollection.length; i++)
		{
			var snake = snakeCollection[i];

			for(var j = snake.length - 1; j >= 0; j--)
			{ 
				if(j===0)
				{
					if (snake[0].d === 0)
					{
						snake[0] = {x: snake[0].x + 1, y: snake[0].y, d: 0, id: snake[0].id, c: snake[0].c};
					}
					else if(snake[0].d === 1)
					{
						snake[0] = {x: snake[0].x - 1, y: snake[0].y, d: 1, id: snake[0].id, c: snake[0].c};
					}
					else if(snake[0].d === 2)
					{
						snake[0] = {x: snake[0].x, y: snake[0].y - 1, d: 2, id: snake[0].id, c: snake[0].c};
					}
					else if(snake[0].d === 3)
					{
						snake[0] = {x: snake[0].x, y: snake[0].y + 1, d: 3, id: snake[0].id, c: snake[0].c};
					}

					detectFood();
					detectSnake(i);

					if(snake[0].x >= 0 && snake[0].y >= 0 &&
						snake[0].x < map.length && snake[0].y < map[0].length)
					{
						map[snake[0].x][snake[0].y] = 2; //draws head
					}
				}
				else
				{
					if(j === (snake.length - 1))	//get rid of last chain in snake
					{
						map[snake[j].x][snake[j].y] = null;
					}
			
					snake[j] = {x: snake[j-1].x, y: snake[j-1].y};
					map[snake[j].x][snake[j].y] = 2;	
				}
			}
		}
	}

	/* ----------------- Websocket Setup  ----------------- */
	var ws;

	function openSocket()
	{
		ws = new WebSocket("ws://web.engr.illinois.edu:8080/~bechapm2/socket");
		//ws = new WebSocket("ws://localhost:8080");
		//ws = new WebSocket("ws://6fd3cf6e.ngrok.com");
		//ws = new WebSocket("ws://echo.websocket.org");
		if("WebSocket" in window)
		{
			ws.onopen = function()
			{
				console.log("handshake confirmed");
			}
			ws.onmessage = function(argument)
			{
				var str = argument.data;
				if(str.indexOf("start") != -1)
				{
					var dataNum = parseInt(str.substring(0,str.indexOf(",")));
					var dataCol = str.substring(str.indexOf("|") + 1);
					console.log(dataCol);

					var found = false;
					for(var i = 0; i < snakeCollection.length; i++)
					{
						//console.log("check");
						var snake = snakeCollection[i];
						console.log(snake[0].id);
						if(snake[0].id === dataNum)
						{
							found = true;
							//console.log("found");
						}
					}

					if(found === false)
					{
						createSnake(dataNum, dataCol);
						//console.log(snakeCollection.length);
					}
				}
				else
				{
					var pgID = parseInt(str.substring(0,str.indexOf(",")));
					var dir = parseInt(str.substring(str.indexOf(",") + 1));

					//console.log(dir);

					for(var i = 0; i < snakeCollection.length; i++)
					{
						var snake = snakeCollection[i];
						if(snake[0].id === pgID)
						{
							if(dir === 2 && snake[0].d !== 3)
							{
								snake[0].d = 2;
							}
							else if(dir === 3 && snake[0].d !== 2)
							{
								snake[0].d = 3;
							}
							else if(dir === 1 && snake[0].d !== 0)
							{
								snake[0].d = 1;
							}
							else if(dir === 0 && snake[0].d !== 1)
							{
								snake[0].d = 0;
							}
						}
					}
				}
			}
			ws.onclose = function()
			{
				console.log("connection terminated");
			}
		}
		else
		{
			alert("game incompatible");
		}
	}

	openSocket();


}